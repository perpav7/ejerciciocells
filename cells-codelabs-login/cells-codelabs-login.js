class CellsCodelabsLogin extends Polymer.Element {

  static get is() {
    return 'cells-codelabs-login';
  }

  static get properties() {
    return {
        /**
         * userName field
         */
        userName: String,
        /**
         * userPassword field
         */
        userPassword: String,
        /**
         * User ID
         */
         userId: String,
         _loading: {
           type: Boolean,
           value: false
         },

         /**
           * Icon ID for the clear text icon.
           */
          clearIcon: {
            type: String,
            value: 'coronita:close'
          },
          /**
           * Icon ID for the show password icon.
           */
          showPwdIcon: {
            type: String,
            value: 'coronita:visualize'
          },
          /**
           * Icon ID for the hide password icon.
           */
          hidePwdIcon: {
            type: String,
            value: 'coronita:hide'
          },

          /**
           * Login status.
           */
          loggedIn: {
            type: Boolean,
            value: false
          },
          /**
           * Text shown in the loading spinner while `loggedIn` property is `false`.
           */
          loggingInText: {
            type: String,
            value: 'Espere un momento...'
          },
          /**
           * Text shown in the loading spinner when `loggedIn` property is `true`.
           */
          loggedInText: {
            type: String,
            value: 'Bienvenido'
          },
          _loadingText: {
            type: String,
            computed: '_computeLoadingText(loggedIn)'
          }
      };
  }

  _onFormSubmit(e) {
    this._loading = true;

    /**
     * Fired after submitting the form
     * @event request-access
     * @param {Object} detail {userId, password, username}
     */
    this.dispatchEvent(new CustomEvent('request-access', {
      composed: true,
      bubbles: true,
      detail: e.detail
    }));
  }
  _computeLoadingText(loggedIn) {
  return loggedIn ? this.loggedInText : this.loggingInText;
}

}

customElements.define(CellsCodelabsLogin.is, CellsCodelabsLogin);
